#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "phasingStat.h"
#include "block.h"
#include "loadFile.h"

/*************************************************
 remove pos. with undertimed haplotype(s)
 Params: hap - the two haplotypes
 Returns: none
*************************************************/
void removeUndeterminedPos(char **hap)
{
	int i;
	int len;

	len = strlen(hap[0]);
	for (i=0; i<len; i++){
		if (hap[0][i]=='-' || hap[1][i]=='-'){
			hap[0][i] = '-';
			hap[1][i] = '-';
		}
	}
}

/***********************************************************************************
 find the pos. with "homozygous" switch error, mask all homo. pos as '-' in testHap2
 Params: realHap - real haplotype (both copy)
		 testHap - test haplotype (both copy)
		 realHap2 - one copy of realHap
		 testHap - one copy of testHap
		 blockOffset - the offset of the testHap in reference of realHap
 Returns: isError[] - 1 if there exists homo. switch error; 0 otherwise
***********************************************************************************/
int* findHomozygousError(char** realHap, char** testHap, char* realHap2, char* testHap2, int blockOffset)
{
	int i;
	int num_snp;
	int* isError;

	num_snp = strlen(testHap[0]);
	isError = (int*)malloc(sizeof(int)*num_snp);
	memset(isError, 0, sizeof(int)*num_snp);

	//case1: sites at which realHap is homozygous
	for (i=0; i<num_snp; i++){
		if (realHap[0][i+blockOffset]=='0' && realHap[1][i+blockOffset]=='0'){
			testHap2[i] = '-';
			if (testHap[0][i]=='1' || testHap[1][i]=='1')
				isError[i] = 1;
		}
		else if (realHap[0][i+blockOffset]=='1' && realHap[1][i+blockOffset]=='1'){
			testHap2[i] = '-';
			if (testHap[0][i]=='0' || testHap[1][i]=='0')
				isError[i] = 1;
		}
	}
	//Case2: sites at which testHap are homozygous 
	for (i=0; i<num_snp; i++){
		if (testHap[0][i]=='0' && testHap[1][i]=='0'){
			testHap2[i] = '-';
			if (realHap[0][i+blockOffset]=='1'|| realHap[1][i+blockOffset]=='1')
				isError[i] = 1;
		}
		else if (testHap[0][i]=='1' && testHap[1][i]=='1'){
			testHap2[i] = '-';
			if (realHap[0][i+blockOffset]=='0' || realHap[1][i+blockOffset]=='0')
				isError[i] = 1;
		}
	}

	/*for (i=0; i<num_snp; i++){
		printf("%d", isError[i]);
	}printf("\n");
	printf("%s\n", testHap2);*/

	return isError;
}

void splitBlock(char** realHap, char** testHap, char* realHap2, char* testHap2, int blockOffset, int* isError, BlockList* blist, int* varPos)
{
	int i;
	int num_snp;
	int lastPos = -1;
	int start = -1;
	int newBlock = 0;//boolean variable
	char lastReal, lastHap;
	char currentReal, currentHap;
	int numPhased;
	int* inBlock;
	int hasSE = 0;

	num_snp = strlen(testHap[0]);

	for (i=0; i<num_snp; i++){
		//a homo. switch error
		if (isError[i] == 1){
			createUnitBlock(blist);
			if (lastPos != -1){
				numPhased = getNumPhased2(testHap, start, lastPos);
				createBlock(blist, varPos, start+blockOffset, lastPos+blockOffset, numPhased);
			}
			start = -1;
			lastPos = -1;
			continue;
		}

		currentHap = testHap2[i];
		currentReal = realHap2[i];
		if (currentHap!='-' && currentReal!='-'){
			if (lastPos >= 0){
				lastHap = testHap2[lastPos];
				lastReal = realHap2[lastPos];
				hasSE = 0;
				if ((lastReal==currentReal && lastHap!=currentHap) || (lastReal!=currentReal && lastHap==currentHap))
					hasSE = 1;
				if (hasSE == 1){
					numPhased = getNumPhased2(testHap, start, lastPos);
					createBlock(blist, varPos, start+blockOffset, lastPos+blockOffset, numPhased);
					start = i;
					lastPos = i;
				}
			}
			lastPos = i;
			if (start == -1)
				start = i;
		}
	}
	if (lastPos != -1){
		numPhased = getNumPhased2(testHap, start, lastPos);
		createBlock(blist, varPos, start+blockOffset, lastPos+blockOffset, numPhased);
	}

}

void partitionBlock(char** realHap, char** testHap, int blockOffset, BlockList* blist, int* varPos)
{
	int i;
	int num_snp;
	int *isError;
	char *realHap2, *testHap2;
	
	num_snp = strlen(testHap[0]);
	removeUndeterminedPos(realHap);
	removeUndeterminedPos(testHap);
	realHap2 = (char*)malloc(sizeof(char)*(num_snp+1));
	testHap2 = (char*)malloc(sizeof(char)*(num_snp+1));
	strcpy(realHap2, "");
	strcpy(testHap2, "");
	strncat(testHap2, testHap[0], num_snp);
	strncat(realHap2, realHap[0]+blockOffset, num_snp);

	//printf("realHap2:%s\n", realHap2);
	//printf("testHap2:%s\n", testHap2);

	isError = findHomozygousError(realHap, testHap, realHap2, testHap2, blockOffset);

	//printf("realHap2:%s\n", realHap2);
	//printf("testHap2:%s\n", testHap2);
	//for (i=0; i<num_snp; i++)
	//	printf("%d", isError[i]);
	//printf("\n");
	splitBlock(realHap, testHap, realHap2, testHap2, blockOffset, isError, blist, varPos);

	free(isError);
	free(realHap2);
	free(testHap2);
	free(testHap[0]);
	free(testHap[1]);
	free(testHap);
}
