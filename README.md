# QAN50

C program to calculate QAN50. 

# Citation

Chen, Zhi-Zhong, **Fei Deng**, Chao Shen, Yiji Wang, and Lusheng Wang. "Better ILP-Based Approaches to Haplotype Assembly." *Journal of Computational Biology*, 23, no. 7 (2016): 537-552.


# How to install

You can easily compiling the program by typing
```
make
```

# How to use
Five parameters should be provided in order to run QAN50

* argv[1]: variation file
* argv[2]: file storing block start position
* argv[3]: TRUE haplotype file
* argv[4]: prefix of filename for each block, e.g., chr22/chr22
* argv[5]: suffix of a block file, 

