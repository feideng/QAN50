#ifndef _BLOCK_STAT_
#define _BLOCK_STAT_

/********************************************
 definition of a block
 Attrs: numVar - #snps in the block
		phased - #phased snps in the block
		span - span of the block
		adjusted span - span*phased/numVar
********************************************/
typedef struct _block{
	int numVar;
	int phased;
	int span;
	double adjustedSpan;
	struct _block *next;
}Block;

/************************************************
 definition of a linked list of blocks
 Attrs: head - head of the list
		tail - tail of the list
		num_block - #blocks in the list
************************************************/
typedef struct _blockList{
	Block *head;
	Block *tail;
	int num_block;
}BlockList;

/*get phased N50*/
int getPhaseN50(Block *blockArr, int num, int totalVar);
/*get span N50*/
int getSpanN50(Block *blockArr, int num, int totalVar);
/*get adjusted span N50*/
double getAdjustedSpanN50(Block *blockArr, int num, int totalVar);
/*print the phasing statistics*/
void printPhasingStat(Block *blockArr, int numBlock, int totalVar);
/*create an empty block list*/
BlockList* createBlockList();
/*add a new block to the block list*/
void addToBlockList(BlockList *blist, Block *ablock);
/*free the space allocated to a block list*/
void freeBlockList(BlockList *blist);
/*transform a block list into the corresponding array representation*/
Block* blockList2Array(BlockList *blist);
/*print a block list*/
void printBlockList(BlockList *blist);
/*create a new block*/
Block* createBlock(BlockList *blist, int *varPos, int firstPos, int lastPos, int num_phased);
/*create a block with a single snp*/
Block* createUnitBlock(BlockList *blist);

/*cut a whole haplotype into blocks*/
void cutChro2Block(char* chrFile);
/*write a haplotype to file*/
void writeHap2File(char* filename, char* hap);

void printBlockDetail(Block *blockArr, int num);

#endif