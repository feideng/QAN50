#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "loadFile.h"
#include "list.h"

/********************************************
 get the number of phased snps
 Params: hap - the two haplotypes
 Return: the #phased
*********************************************/
int getNumPhased(char **hap)
{
	int i;
	int num = 0;
	
	//require both haplotype to be non'-'
	for (i=0; i<strlen(hap[0]); i++){
		if (hap[0][i]!='-' && hap[1][i]!='-')
			num++;
	}

	return num;
}

/********************************************************
 get the number of phased snps in a range
 Params: hap - the two haplotpyes
		 first - the first pos
		 lastPos - the last pos
 Return: the #phased
*********************************************************/
int getNumPhased2(char **hap, int firstPos, int lastPos)
{
	int i, num=0;
	int len;

	len = strlen(hap[0]);
	if (firstPos>=len || lastPos>=len || firstPos<0 || lastPos<0 || firstPos>lastPos){
		printf("Error:wrong range!\n");
		exit(0);
	}

	for (i=firstPos; i<=lastPos; i++){
		if (hap[0][i]!='-' && hap[1][i]!='-')
			num++;
	}

	return num;
}

/******************************************************
 load the two haplotypes from file
 Params: hapFile - the haplotype file
 Return: the two haplotypes
******************************************************/
char** loadHaplotype_general(char* hapFile)
{
	int i;
	char chr;
	char **hap;
	int num_snp = 0;
	FILE *fin;

	fin = fopen(hapFile, "r");
	if (fin == NULL){
		printf("Error:cannot open file %s!\n", hapFile);
		exit(0);
	}

	//get the length of each haplotype
	chr = fgetc(fin);
	while (chr!=EOF && chr!='\n' && chr!='\r'){
		if (chr=='0' || chr=='1' || chr=='-'){
			num_snp++;
			chr = fgetc(fin);
		}
		else{
			printf("Error:unknown character in file %s!\n", hapFile);
			exit(0);
		}
	}

	//allocate memory
	hap = (char**)malloc(sizeof(char*)*2);
	for (i=0; i<2; i++){
		hap[i] = (char*)malloc(sizeof(char)*(num_snp+1));
		strcpy(hap[i], "");
	}

	fseek(fin, 0, SEEK_SET);
	fscanf(fin, "%s", hap[0]);
	fscanf(fin, "%s", hap[1]);
	if (strlen(hap[0]) != strlen(hap[1])){
		printf("Error:two haplotypes with different lengths!\n");
		exit(0);
	}

	fclose(fin);
	return hap;
}

/****************************************************************
 load the variant from file
 Params: variantFile - the variant file
	     pos - in which column the variant is stored
		 totalVar - the variable used to store the #variants
 Returns: varPos - a vector storing the variantpos of each snp
****************************************************************/
int* loadVariant(char *variantFile, int pos, int *totalVar)
{
	int i, col = 0;
	int status = 0;
	int num_snp = 0;
	List *varList;
	FILE *fin;
	char s[1000];
	char chr;
	int *varPos;
	Node *node;

	fin = fopen(variantFile, "r");
	if (fin == NULL){
		printf("Error:cannnot open file %s!\n", variantFile);
		exit(0);
	}

	//store the contents into varList
	varList = createList();
	status = fscanf(fin, "%s", s);
	while (status != EOF){
		col++;
		if (col == pos){
			addToList(varList, atoi(s));
			num_snp++;
		}
		chr = fgetc(fin);
		while (chr!='\n' && chr!='\r'){
			status = fscanf(fin, "%s", s);
			col++;
			if (col == pos){
				addToList(varList, atoi(s));
				num_snp++;
			}
			chr = fgetc(fin);
		}
		col = 0;
		status = fscanf(fin, "%s", s);
	}

	//restore varList into array varPos
	i = 1;//index of variant starts from 1
	*totalVar = num_snp;
	varPos = (int*)malloc(sizeof(int)*(num_snp+1));
	memset(varPos, 0, sizeof(int)*(num_snp+1));
	node = varList->head->next;
	while (node != NULL){
		varPos[i++] = node->index;
		node = node->next;
	}

	fclose(fin);
	free(varList);
	return varPos;
}

/*************************************************************
 test function, generate a pair of random haplotpes
 Params: size - #snps of each haplotpe
		 testHapFile - file name for one pair of haplotypes
		 realHapFile - file name for another pair of haplotypes
**************************************************************/
void generateRandomHaplotype(int size, char* testHapFile, char* realHapFile)
{
	int i;
	int num;
	char* hap;
	FILE *f1, *f2;

	f1 = fopen(testHapFile, "w+");
	f2 = fopen(realHapFile, "w+");
	if (f1==NULL || f2==NULL){
		printf("Error:cannot open file!\n");
		exit(0);
	}

	hap = (char*)malloc(sizeof(char)*(size+1));
	strcpy(hap, "");

	srand(time(NULL));
	//generate two test haplotypes
	for (i=0; i<size; i++){
		num = rand()%3;
		if (num == 0) strncat(hap, "0", 1);
		else if (num == 1) strncat(hap, "1", 1);
		else strncat(hap, "-", 1);
	}
	fprintf(f1, "%s\n", hap);
	strcpy(hap, "");
	for (i=0; i<size; i++){
		num = rand()%3;
		if (num == 0) strncat(hap, "0", 1);
		else if (num == 1) strncat(hap, "1", 1);
		else strncat(hap, "-", 1);
	}
	fprintf(f1, "%s\n", hap);
	
	//generate two real haplotypes
	strcpy(hap, "");
	for (i=0; i<size; i++){
		num = rand()%3;
		if (num == 0) strncat(hap, "0", 1);
		else if (num == 1) strncat(hap, "1", 1);
		else strncat(hap, "-", 1);
	}
	fprintf(f2, "%s\n", hap);
	strcpy(hap, "");
	for (i=0; i<size; i++){
		num = rand()%3;
		if (num == 0) strncat(hap, "0", 1);
		else if (num == 1) strncat(hap, "1", 1);
		else strncat(hap, "-", 1);
	}
	fprintf(f2, "%s\n", hap);

	fclose(f1);
	fclose(f2);

}


/***********************************************************
 load the start pos. of each block
 Params: posFile - the disk file, one line per each block
		 numBlock - used to store #blocks
 Returns: an array storing the start pos. of each block
***********************************************************/
int* loadBlockStartPos(char* posFile, int* numBlock)
{
	int status = 0;
	int count = 0;
	char s[100];
	FILE *fin;
	int* blockStartPos;
	
	fin = fopen (posFile, "r");
	if (fin == NULL){
		printf("Error:cannot open file!\n");
		exit(0);
	}

	//count #blocks
	status = fscanf(fin, "%s", s);
	while (status != EOF){
		count++;
		status = fscanf(fin, "%s", s);
	}
	*numBlock = count;
	
	//get the startPos of each block
	blockStartPos = (int*)malloc(sizeof(int)*count);
	fseek(fin, 0, SEEK_SET);
	count = 0;
	status = fscanf(fin, "%s", s);
	while (status != EOF){
		blockStartPos[count++] = atoi(s);
		status = fscanf(fin, "%s", s);
	}

	fclose(fin);
	return blockStartPos;
}
