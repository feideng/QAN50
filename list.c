#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "list.h"


/*create an empty list*/
List* createList()
{
	Node *node;
	List *nList;

	nList = (List *)malloc(sizeof(List));
	node = (Node *)malloc(sizeof(Node));
	node->next = NULL;
	nList->head = node;
	nList->tail = node;

	return nList;
}

/*********************************************
 free a list
 Params: aList - the list
 Returns: none
*********************************************/
void freeList(List *aList)
{
	Node *p, *pp;

	p = aList->head;
	if (p == NULL)
		return;
	pp = p->next;
	while (pp != NULL){
		free(p);
		p = pp;
		pp = pp->next;
	}
	free(p);
}

/***********************************************
 add a node to the tail of a given list
 Params: aList - the given list
		 index - the value of the node to be added
 Returns: none
************************************************/
void addToList(List *aList, int index)
{
	Node *node;
	node = (Node*)malloc(sizeof(Node));
	node->index = index;
	node->next = NULL;
	
	if (aList->head == aList->tail){
		aList->head->next = node;
		aList->tail = node;
	}
	else {
		aList->tail->next = node;
		aList->tail = node;
	}
}

/*******************************************
 print the content of a list
*******************************************/
void printList(List *aList)
{
	Node *node;
	printf("\nPrinting list:\n");
	node = aList->head->next;
	while (node != NULL){
		printf("%d\n", node->index);
		node = node->next;
	}
	//printf("\n");
}