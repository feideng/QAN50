#ifndef _LOADFILE_
#define _LOADFILE_

int getNumPhased(char **hap);
int getNumPhased2(char **hap, int firstPos, int lastPos);
int* loadVariant(char *variantFile, int pos, int *totalVar);
char* loadHaplotype_het(char* hapFile);
char** loadHaplotype_general(char* hapFile);
/*load the start pos.of each block*/
int* loadBlockStartPos(char* posFile, int* numBlock);
void generateRandomHaplotype(int size, char* testHapFile, char* realHapFile);



#endif