#ifndef _PHASINGSTAT_
#define _PHASINGSTAT_

#include "block.h"

void removeUndeterminedPos(char **hap);
int* findHomozygousError(char** realHap, char** testHap, char* realHap2, char* testHap2, int blockOffset);
void partitionBlock(char** realHap, char** testHap, int blockOffset, BlockList* blist, int* varPos);
void splitBlock(char** realHap, char** testHap, char* realHap2, char* testHap2, int blockOffset, int* isError, BlockList* blist, int* varPos);


#endif