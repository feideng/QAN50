#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "block.h"
#include "loadFile.h"

//static int phasedComparator(const Block *a, const Block *b);
//static int spanComparator(const void *a, const void *b);
//static int adjustedSpanComparator(const void *a, const void *b);


/************************************************************
 compare the #phased of two blocks
 Params: *a - block a
		 *b - block b
 Returns: 1 - if b.phased > a.phased
		  -1 - if b.phased < a.phased
		  0 - otherwise
************************************************************/
static int phasedComparator(const void *a, const void *b)
{
	int bb = ((Block*)b)->phased;
	int aa = ((Block*)a)->phased;
	if (bb > aa) return 1;
	else if (bb < aa) return -1;
	else return 0;
}

/************************************************************
 compare the span of two blocks
 Params: *a - block a
		 *b - block b
 Returns: 1 - if b.span > a.span
		  -1 - if b.span < a.span
		  0 - otherwise
************************************************************/
static int spanComparator(const void *a, const void *b)
{
	int bb = ((Block*)b)->span;
	int aa = ((Block*)a)->span;
	if (bb > aa) return 1;
	else if (bb < aa) return -1;
	else return 0;
}

/************************************************************
 compare the adjusted span of two blocks
 Params: *a - block a
		 *b - block b
 Returns: 1 - if b.adjustedSpan > a.adjustedSpan
		  -1 - if b.adjustedSpan < a.adjustedSpan
		  0 - otherwise
************************************************************/
static int adjustedSpanComparator(const void *a, const void *b)
{
	double bb = ((Block*)b)->adjustedSpan;
	double aa = ((Block*)a)->adjustedSpan;
	if (bb > aa) return 1;
	else if (bb < aa) return -1;
	else return 0;
}


/*************************************************************
 get phased N50
 @Params: blockList - array containing blocks of statistic
          num - #blocks
		  totalVar - total number of variants
 Returns: none
*************************************************************/
int getPhaseN50(Block *blockArr, int num, int totalVar)
{
	int i;
	int sum = 0;

	//sort blocks in decreasing order of #phased
	qsort(blockArr, num, sizeof(Block), phasedComparator);
	for (i=0; i<num; i++){
		sum += blockArr[i].phased;
		if (sum > totalVar/2)
			return blockArr[i].phased;
	}
	return 0;
}

/*************************************************************
 get span N50
 Params: blockList - array containing blocks of statistic
         num - #blocks
		 totalVar - total number of variants
 Returns: none
*************************************************************/
int getSpanN50(Block *blockArr, int num, int totalVar)
{
	int i;
	int sum = 0;

	//sort blocks in decreasing order of span
	qsort(blockArr, num, sizeof(Block), spanComparator);
	for (i=0; i<num; i++){
		sum += blockArr[i].phased;
		if (sum > totalVar/2)
			return blockArr[i].span;
	}
	return 0;
}

/*************************************************************
 get adjustedSpan N50
 @Params: blockList - array containing blocks of statistic
          num - #blocks
		  totalVar - total number of variants
 Returns: none
*************************************************************/
double getAdjustedSpanN50(Block *blockArr, int num, int totalVar)
{
	int i;
	int sum = 0;

	//sort blocks in decreasing order of adjusted span
	qsort(blockArr, num, sizeof(Block), adjustedSpanComparator);
	for (i=0; i<num; i++){
		sum += blockArr[i].phased;
		if (sum > totalVar/2)
			return blockArr[i].adjustedSpan;
	}
	return 0;
}

/*****************************************************************
 print the phasing statistics, i.e. phasedN50, spanN50 and AN50
 Params: blockArr - array containing blocks of statistic
		 num - #blocks
		 totalVar - total number of variants
 Returns: none
*****************************************************************/
void printPhasingStat(Block *blockArr, int num, int totalVar)
{
	int phaseN50;
	int spanN50;
	double adjustedSpanN50;

	phaseN50 = getPhaseN50(blockArr, num, totalVar);
	spanN50 = getSpanN50(blockArr, num, totalVar);
	adjustedSpanN50 = getAdjustedSpanN50(blockArr, num, totalVar);

	//printf("\nPhasing results:\n");
	//printf("phasedN50:%d\n", phaseN50);
	//printf("spanN50:%d\n", spanN50);
	//printf("adjustedSpanN50:%f\n", adjustedSpanN50);
	printf("%f\n", adjustedSpanN50);
}

/*create an empty blockList*/
BlockList* createBlockList()
{
	Block *bnode;
	BlockList *blist;

	bnode = (Block*)malloc(sizeof(Block));
	bnode ->next = NULL;
	blist = (BlockList*)malloc(sizeof(BlockList));
	blist->head = bnode;
	blist->tail = bnode;
	blist->num_block = 0;

	return blist;
}

/*******************************************************
 add a block to the tail of the given block list
 Params: blist - a given block list
		 ablock - the block to be added
 Returns: none
*******************************************************/
void addToBlockList(BlockList *blist, Block *ablock)
{
	if (blist->head == blist->tail){
		blist->head->next = ablock;
		blist->tail = ablock;
	}
	else {
		blist->tail->next = ablock;
		blist->tail = ablock;
	}

	blist->num_block++;
}

/***************************************************
 free the memory allocated to a block list
 Params: blist - the block list to be freed
 Returns: none
****************************************************/
void freeBlockList(BlockList *blist)
{
	Block *p, *pp;

	p = blist->head;
	if (p == NULL)
		return;
	pp = p->next;
	while (pp != NULL){
		free(p);
		p = pp;
		pp = pp->next;
	}
	free(p);
}

/*******************************************************************************************
 create a new block and add it to a given block list
 Params: blist - the given block list
		 varPos - the array storing the pos (in reference of whole chromosome) of each snp
		 firstPos - the first pos. of the block
		 lastPos - the last pos. of the block
		 num_phased - #phased snps in the block
 Returns: the newly created block
********************************************************************************************/
Block* createBlock(BlockList *blist, int *varPos, int firstPos, int lastPos, int num_phased)
{
	int i;
	int span;
	int size;
	int phased;
	double adjustedSpan;
	Block *ablock;
	
	//calculate corresponding attributes
	size = lastPos-firstPos+1;
	span = varPos[lastPos+1]-varPos[firstPos+1]+1;//snp index starts from 1, whereas
												 //firstPos& lastPos are indexes start from 0
	phased = num_phased;
	adjustedSpan = (double) span*phased*1.0/size;

	//create new block with the calculated attributes
	ablock = (Block*)malloc(sizeof(Block));
	ablock->next = NULL;
	ablock->adjustedSpan = adjustedSpan;
	ablock->numVar = size;
	ablock->phased = phased;
	ablock->span = span;
	addToBlockList(blist, ablock);

	return ablock;
}

/**************************************************************
 create a unit block with a single snp
 Params: blist - the list to which the block is added
 Returns: the created block
**************************************************************/
Block* createUnitBlock(BlockList *blist)
{
	Block *ablock;
	ablock = (Block*)malloc(sizeof(Block));
	ablock->next = NULL;
	ablock->numVar = 1;
	ablock->phased = 1;
	ablock->span = 1;
	ablock->adjustedSpan = 1;
	addToBlockList(blist, ablock);

	return ablock;
}

/***************************************************************
 transform a blocklist to the corresponding array representation
 Params: blist - the given block list
 Return : the corresponding array representation
****************************************************************/
Block* blockList2Array(BlockList *blist)
{
	int i = 0;
	int num_block;
	Block *blockArr;
	Block *bnode;

	num_block = blist->num_block;
	blockArr = (Block*)malloc(sizeof(Block)*num_block);

	bnode = blist->head->next;
	while (bnode != NULL){
		blockArr[i++] = *bnode;
		bnode = bnode->next;
	}

	return blockArr;
}

/**********************************************
 test function, print a block list
 Params: blist - the given block list
 Returns: none
**********************************************/
void printBlockList(BlockList *blist)
{
	Block *bnode;

	printf("\n===printing block list===\n");
	printf("#blocks:%d\n", blist->num_block);

	bnode = blist->head->next;
	while (bnode != NULL){
		printf("phased:%d  size:%d  span:%d  an:%f\n", bnode->phased, bnode->numVar, bnode->span, bnode->adjustedSpan);
		bnode = bnode->next;
	}
	printf("\n\n");
}

/**********************************************************
 test function, used to write a haplotype to disk
 Params: filename - file name to which writing is operate
		 hap - haplotype to be written
 Returns: none
**********************************************************/
void writeHap2File(char* filename, char* hap)
{
	FILE *fp;

	fp = fopen(filename, "a+");
	if (fp==NULL){
		printf("Error:cannot open file!\n");
		exit(0);
	}
	fprintf(fp, "%s\n", hap);

	fclose(fp);
}

/************************************************************
 test function, suppose we have two solutions for the same
 chromosome, one in the form of a single hap. while the other
 in the form of blocks, this method cut the former into 
 blocks based on the info. of the latter solution.
 Params: chrFile - the single hapFile
         posFile - the file for the startPos of each block
**********************************************************/
void cutChro2Block(char* chrFile)
{
	int i;
	int len;
	int numBlock;
	int* blockStartPos;
	char testHapFile[100];
	char blockFile[100];
	char** chrHap, **testHap;
	char* temp;

	blockStartPos = loadBlockStartPos("part_chr1_22/chr22/chr22.pos", &numBlock);
	//load the haplotype for the chromosome
	chrHap = loadHaplotype_general(chrFile);
	len = strlen(chrHap[0]);
	temp = (char*)malloc(sizeof(char)*(len+1));

	for (i=0; i<numBlock; i++){
		strcpy(testHapFile, "");
		sprintf(testHapFile, "part_chr1_22/chr22/chr22_%d.hap", i);
		strcpy(blockFile, "");
		sprintf(blockFile, "mec_chr1_22/chr22_mec/chr22_4c_%d.hap", i);

		testHap = loadHaplotype_general(testHapFile);
		len = strlen(testHap[0]);
		//the first copy
		strcpy(temp, "");
		strncat(temp, chrHap[0]+blockStartPos[i]-1, len);
		writeHap2File(blockFile, temp);
		//the other copy
		strcpy(temp, "");
		strncat(temp, chrHap[1]+blockStartPos[i]-1, len);
		writeHap2File(blockFile, temp);
	}

	free(temp);
}

/**
test function, print block detail
*/
void printBlockDetail(Block *blockArr, int num)
{
	int i = 0;
	FILE* fp = fopen("blockDetail", "w");
	qsort(blockArr, num, sizeof(Block), adjustedSpanComparator);
	for (i=0; i<num; i++){
		fprintf(fp, "%f\n", blockArr[i].adjustedSpan);
	}

	fclose(fp);
}

