CC = gcc
CFLAGS  = -g -Wall

default: QAN50

QAN50:  main.o block.o list.o loadFile.o phasingStat.o 
	$(CC) $(CFLAGS) -o QAN50 block.o list.o loadFile.o phasingStat.o main.o


main.o:  main.c phasingStat.h block.h loadFile.h
	$(CC) $(CFLAGS) -c main.c

block.o:  block.c block.h loadFile.h 
	$(CC) $(CFLAGS) -c block.c

list.o:  list.c list.h 
	$(CC) $(CFLAGS) -c list.c

loadFile.o:  loadFile.c loadFile.h 
	$(CC) $(CFLAGS) -c loadFile.c

phasingStat.o:  phasingStat.c phasingStat.h block.h loadFile.h
	$(CC) $(CFLAGS) -c phasingStat.c
		
clean: 
	$(RM) QAN50 *.o *~