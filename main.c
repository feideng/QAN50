#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "loadFile.h"
#include "block.h"
#include "phasingStat.h"


/*
 argv[1]: variation file
 argv[2]: file storing block start position
 argv[3]: TRUE haplotype file
 argv[4]: prefix of filename for each block, e.g., chr22/chr22
 argv[5]: suffix of a block file, 
*/

int main(int argc, char** argv)
{
	int i;
	int totalVar;
	int numBlock;
	int* varPos;
	int* blockStartPos;
	BlockList* blist;
	Block* barray;
	char** realHap, **testHap;
	char testHapFile[100];
	int sum = 0;

	if (argc != 6){
		printf("Usage: ./prog_name variationFile blockStartPositionFile trueHaplotypeFile  prefix_of_block_name  suffix_of_block_name\n");
		exit(0);
	}

	blist = createBlockList();
	varPos = loadVariant(argv[1], 1, &totalVar);
	blockStartPos = loadBlockStartPos(argv[2], &numBlock);
	realHap = loadHaplotype_general(argv[3]);

	for (i=0; i<numBlock; i++){
		strcpy(testHapFile, "");
		sprintf(testHapFile, "%s_%d.%s", argv[4], i+1, argv[5]);
		testHap = loadHaplotype_general(testHapFile);
		partitionBlock(realHap, testHap, blockStartPos[i]-1, blist, varPos);
	}

	numBlock = blist->num_block;
	barray = blockList2Array(blist);
	for (i=0; i<numBlock; i++)
		sum += barray[i].numVar;
	printPhasingStat(barray, numBlock, totalVar);


	return 0;

}


