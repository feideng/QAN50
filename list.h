#ifndef _LIST_H_
#define _LIST_H_

/****************************************
 structure definition of linked node
 Attribute: index - the value
			next - the pointer to next
****************************************/
typedef struct _node{
	int index;
	struct _node *next;
}Node;


/***************************************
 definition of linked list
 Attribute: head - head node
			tail - tail node
***************************************/
typedef struct _list{
	Node *head;
	Node *tail;
}List;

/*create a linked list*/
List* createList();
/*free a list*/
void freeList(List *aList);
/*add a node to a list*/
void addToList(List *aList, int index);
/*print a list*/
void printList(List *aList);


#endif